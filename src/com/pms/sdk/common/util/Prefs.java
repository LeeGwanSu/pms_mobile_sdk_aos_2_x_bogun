package com.pms.sdk.common.util;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Build;

import com.pms.sdk.IPMSConsts;

/**
 * 
 * @author erzisk
 * @since 2013.06.11
 */
public class Prefs implements IPMSConsts {

	private final SharedPreferences prefs;
	private final Editor edit;

	@SuppressLint("CommitPrefEdits")
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	public Prefs(Context context) {
		prefs = context.getSharedPreferences(PREF_FILE_NAME, Activity.MODE_MULTI_PROCESS);
		edit = prefs.edit();
	}

	public void putString (String k, String v) {
		try {
			edit.putString(k, v);
		} catch (Exception e) {
			CLog.e(e.getMessage());
		}
		edit.commit();
	}

	public String getString (String k) {

		try {
			return prefs.getString(k, "");
		} catch (Exception e) {
			CLog.e(e.getMessage());
			return "";
		}

	}

	public void putInt (String k, int v) {

		try {
			edit.putInt(k, v);
		} catch (Exception e) {
			CLog.e(e.getMessage());
		}
		edit.commit();
	}

	public int getInt (String k) {

		try {
			return prefs.getInt(k, -1);
		} catch (Exception e) {
			CLog.e(e.getMessage());
		}
		return -1;

	}

	public void putBoolean (String k, boolean v) {

		try {
			edit.putBoolean(k, v);
		} catch (Exception e) {
			CLog.e(e.getMessage());
		}

		edit.commit();
	}

	public Boolean getBoolean (String k) {

		try {
			return prefs.getBoolean(k, false);
		} catch (Exception e) {
			CLog.e(e.getMessage());
		}
		return false;
	}
}
