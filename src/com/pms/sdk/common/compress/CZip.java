package com.pms.sdk.common.compress;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import com.pms.sdk.common.util.CLog;

public class CZip {

	public static byte[] zipStringToBytes (String input, String charSet) throws IOException {

		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		GZIPOutputStream gzipOutputStream = new GZIPOutputStream(byteArrayOutputStream);
		BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(gzipOutputStream);
		bufferedOutputStream.write(input.getBytes(charSet));

		bufferedOutputStream.close();
		byteArrayOutputStream.close();

		return byteArrayOutputStream.toByteArray();
	}

	public static byte[] zipStringToBytes (String input) throws IOException {
		return zipStringToBytes(input, "UTF-8");
	}

	public static String unzipStringFromBytes (byte[] bytes) throws IOException {

		ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes);
		GZIPInputStream gzipInputStream = new GZIPInputStream(byteArrayInputStream);
		BufferedInputStream bufferedInputStream = new BufferedInputStream(gzipInputStream);
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

		byte[] buffer = new byte[100];
		int length;
		while ((length = bufferedInputStream.read(buffer)) > 0) {
			byteArrayOutputStream.write(buffer, 0, length);
		}
		bufferedInputStream.close();
		gzipInputStream.close();
		byteArrayInputStream.close();
		byteArrayOutputStream.close();

		return byteArrayOutputStream.toString();
	}

	public static boolean unzip (String srcPath, String dstPath) {

		if (srcPath == null || dstPath == null) {
			return false;
		}

		ZipInputStream zis = null;
		FileOutputStream fos = null;
		ZipEntry entry = null;
		int bytes_read;
		byte[] buf = new byte[4096];

		try {
			File dir = new File(dstPath);
			if (!dir.exists())
				dir.mkdirs();
		} catch (Exception e) {
			CLog.e(e.getMessage());
		}

		try {

			zis = new ZipInputStream(new FileInputStream(srcPath));
			boolean esc = false;

			while (true) {

				entry = zis.getNextEntry();
				String nm = entry.getName();
				fos = new FileOutputStream(dstPath + "/" + nm);

				while ((bytes_read = zis.read(buf)) != -1) {
					fos.write(buf, 0, bytes_read);
				}

				fos.close();

				if (esc)
					break;
				else if (zis.available() != 1)
					esc = true;

			}

		} catch (Exception e) {
			CLog.e(e.getMessage());
			return false;
		} finally {

			try {
				zis.close();
			} catch (Exception e) {
				CLog.e(e.getMessage());
			}
		}

		return true;
	}

}
