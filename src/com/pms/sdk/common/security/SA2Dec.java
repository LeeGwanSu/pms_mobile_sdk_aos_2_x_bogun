package com.pms.sdk.common.security;

import com.pms.sdk.common.compress.CZip;
import com.pms.sdk.common.util.CLog;

public class SA2Dec {

	private static String _USER_KEY = "amailRND2009";

	public static final int KEY_LENGTH = 16;
	public static final int KEY_START_INDEX = 17;

	public static String decrypt (String str) {
		return decrypt(str, _USER_KEY);
	}

	public static String decrypt (String str, String userKey) {

		if (str == null || userKey == null || str.length() < KEY_LENGTH + KEY_START_INDEX)
			return null;

		try {

			byte[] decStr = userKey == null ? Seed.seedDecrypt(Base64.decode(str)) : Seed.seedDecrypt(Base64.decode(str), userKey.getBytes());
			return CZip.unzipStringFromBytes(decStr);

		} catch (Exception e) {
			CLog.e(e.getMessage());
			return null;
		} finally {

		}
	}
}
