package com.pms.sdk.common.security;

import com.pms.sdk.common.compress.CZip;
import com.pms.sdk.common.util.CLog;

public class SA2Enc {

	private static String _USER_KEY = "amailRND2009";

	public static String generateKey (String key) {

		String freeKey = String.valueOf(20091022 * (Integer.parseInt(("" + System.currentTimeMillis()).substring(8)))) + "59483286493";
		String genKey = null;

		if (key == null) {
			genKey = freeKey.substring(0, 16);
		} else {

			if (key.length() > 16) {
				genKey = key.substring(0, 16);
			} else {
				genKey = key + (freeKey.substring(0, 16 - key.length()));
			}
		}
		return genKey;
	}

	public static String encode (String str, String userKey) {
		return encode(str, userKey, "UTF-8");
	}

	public static String encode (String str, String userKey, String charSet) {

		if (str == null || userKey == null || charSet == null)
			return null;

		String key = userKey.getBytes().length < 16 ? userKey + _USER_KEY.substring(0, 16 - userKey.getBytes().length) : userKey;

		try {

			byte[] enc1 = CZip.zipStringToBytes(str, charSet);

			byte[] enc2 = key == null ? Seed.seedEncrypt(enc1) : Seed.seedEncrypt(enc1, key.getBytes());

			return Base64.encodeBytes(enc2);

		} catch (Exception e) {
			CLog.e(e.getMessage());
			return null;
		}
	}
}
