package com.pms.sdk.push.mqtt;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.pms.sdk.IPMSConsts;
import com.pms.sdk.common.util.CLog;
import com.pms.sdk.common.util.FileUtil;
import com.pms.sdk.common.util.PMSUtil;

/**
 * Private PUSH Restart Receiver
 * 
 * @author erzisk
 * 
 */
public class RestartReceiver extends BroadcastReceiver implements IPMSConsts {

	private static final String TAG = "MQTT";

	@Override
	public synchronized void onReceive (Context context, Intent intent) {
		CLog.d(TAG, "onReceive() " + intent);
		FileUtil fileUtil = new FileUtil();
		try {
			fileUtil.getConnectLogUtil(context, 5, ((intent.getAction() != null) ? intent.getAction() : "null"));
		} catch (Exception e) {
			CLog.e(e.getMessage());
		}

		CLog.i(TAG, "MQTT_FLAG -> " + PMSUtil.getMQTTFlag(context) + ", PRIVATE_FLAG -> " + PMSUtil.getPrivateFlag(context));
		if (FLAG_Y.equals(PMSUtil.getMQTTFlag(context)) && FLAG_Y.equals(PMSUtil.getPrivateFlag(context))) {
			String action = intent.getAction();
			if (action == null) {
				MQTTService.actionStart(context.getApplicationContext());
			} else {
				if (action.equals(ACTION_BOOT_COMPLETED) || action.equals(ACTION_ACTION_PACKAGE_RESTARTED)) {
					MQTTService.actionStart(context.getApplicationContext());
				} else if (action.equals(ACTION_CHANGERECONNECT)) {
					MQTTService.actionNetworkChangeRestart(context.getApplicationContext());
				} else if (action.equals(ACTION_STOP)) {
					MQTTService.actionStop(context.getApplicationContext());
				} else if (action.equals(ACTION_USER_PRESENT)) {
					Intent i = new Intent(context, MQTTService.class);
					i.setAction(ACTION_FORCE_START);
					context.startService(i);
				} else if(action.equals(ACTION_FORCE_START)) {
					Intent i = new Intent(context, MQTTService.class);
					i.setAction(ACTION_FORCE_START);
					context.startService(i);
				}
			}
		}
	}
}
