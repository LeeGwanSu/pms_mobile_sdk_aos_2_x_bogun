package com.pms.sdk.push.mqtt;

import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttPingSender;
import org.eclipse.paho.client.mqttv3.internal.ClientComms;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.WakefulBroadcastReceiver;

import com.pms.sdk.common.util.CLog;
import com.pms.sdk.common.util.FileUtil;

/**
 * Default ping sender implementation on Android. It is based on AlarmManager.
 * 
 * <p>
 * This class implements the {@link MqttPingSender} pinger interface allowing applications to send ping packet to server every keep alive interval.
 * </p>
 * 
 * @see MqttPingSender
 */
public class AlarmPingSender implements MqttPingSender {

	private static final String TAG = "MQTT_PING";

	private static int mnKeepAliveCount = 1;

	private ClientComms comms;

	private final MQTTService service;

	private WakefulBroadcastReceiver alarmReceiver;

	private final AlarmPingSender that;

	private PendingIntent pendingIntent;

	private volatile boolean hasStarted = false;

	public AlarmPingSender(MQTTService service) {
		if (service == null) {
			throw new IllegalArgumentException("Neither service nor client can be null.");
		}
		this.service = service;
		that = this;
	}

	@Override
	public void init (ClientComms comms) {
		this.comms = comms;
		this.alarmReceiver = new AlarmReceiver();
	}

	@Override
	public void start () {
		String action = MQTTService.PING_SENDER + comms.getClient().getClientId();
		CLog.d(TAG, "Register alarmreceiver to MqttService " + action);
		service.registerReceiver(alarmReceiver, new IntentFilter(action));

		pendingIntent = PendingIntent.getBroadcast(service, 0, new Intent(action), PendingIntent.FLAG_UPDATE_CURRENT);

		schedule(comms.getKeepAlive());
		hasStarted = true;
	}

	@Override
	public void stop () {
		AlarmManager alarmManager = (AlarmManager) service.getSystemService(Service.ALARM_SERVICE);
		alarmManager.cancel(pendingIntent);

		CLog.d(TAG, "Unregister alarmreceiver to MqttService " + comms.getClient().getClientId());
		if (hasStarted) {
			hasStarted = false;
			try {
				service.unregisterReceiver(alarmReceiver);
				mnKeepAliveCount = 1;
			} catch (IllegalArgumentException e) {
			}
		}
	}

	@Override
	public void schedule (long delayInMilliseconds) {
		long nextAlarmInMilliseconds = System.currentTimeMillis() + delayInMilliseconds;
		CLog.d(TAG, "Schedule next alarm at " + nextAlarmInMilliseconds);
		AlarmManager alarmManager = (AlarmManager) service.getSystemService(Service.ALARM_SERVICE);
		alarmManager.set(AlarmManager.RTC_WAKEUP, nextAlarmInMilliseconds, pendingIntent);
	}

	class AlarmReceiver extends WakefulBroadcastReceiver {
		private final String wakeLockTag = MQTTService.PING_WAKELOCK + that.comms.getClient().getClientId();

		@Override
		public void onReceive (Context context, Intent intent) {
			int count = intent.getIntExtra(Intent.EXTRA_ALARM_COUNT, -1);
			CLog.d(TAG, "Ping " + count + " times.");

			CLog.d(TAG, "Check time :" + System.currentTimeMillis());
			IMqttToken token = comms.checkForActivity();

			if (token == null) {
				return;
			}

			try {
				FileUtil file = new FileUtil();
				file.getConnectLogUtil(context, 7, wakeLockTag, mnKeepAliveCount++);
			} catch (Exception e) {
				CLog.e(e.getMessage());
			}

			token.setActionCallback(new IMqttActionListener() {
				@Override
				public void onSuccess (IMqttToken asyncActionToken) {
					CLog.d(TAG, "Success. Release lock(" + wakeLockTag + "):" + System.currentTimeMillis());
				}

				@Override
				public void onFailure (IMqttToken asyncActionToken, Throwable exception) {
					CLog.d(TAG, "Failure. Release lock(" + wakeLockTag + "):" + System.currentTimeMillis());
				}
			});
		}
	}
}
