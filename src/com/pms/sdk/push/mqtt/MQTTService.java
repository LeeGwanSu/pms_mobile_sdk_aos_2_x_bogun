package com.pms.sdk.push.mqtt;

import java.io.ByteArrayInputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Timer;
import java.util.TimerTask;

import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttAsyncClient;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttSecurityException;
import org.eclipse.paho.client.mqttv3.persist.MqttDefaultFilePersistence;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.IBinder;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;

import com.pms.sdk.IPMSConsts;
import com.pms.sdk.api.APIManager.APICallback;
import com.pms.sdk.api.request.GetSignKey;
import com.pms.sdk.bean.Logs;
import com.pms.sdk.common.util.CLog;
import com.pms.sdk.common.util.DateUtil;
import com.pms.sdk.common.util.FileUtil;
import com.pms.sdk.common.util.PMSUtil;
import com.pms.sdk.common.util.StringUtil;
import com.pms.sdk.db.PMSDB;

/**
 * MQTT Private PUSH Service
 * 
 * @author Yang
 * @since 2013.11.26
 */
public class MQTTService extends Service implements IPMSConsts, MqttCallback {

	private static final String TAG = "MQTT";

	// intent, key
	public static final String INTENT_RECEIVED_MSG = "org.mosquitto.android.mqtt.MSGRECVD";
	public static final String KEY_MSG = "org.mosquitto.android.mqtt.MSG";
	public static final String PREF_APP_FIRST = "mqtt_first";
	public static final String PREF_RETRY = "retryInterval";
	public static final String PREF_MQTT_STATUS = "pref_mqtt_status";
	public static final String PREF_KEEPALIVE_INTERVAL = "keepalive_interval";

	// preferences info
	private static final String PREF_FILE_NAME = "mqttconnectioninfo.pref";

	// Intent prefix for Ping sender.
	public static final String PING_SENDER = TAG + ".pingSender.";

	// Constant for wakelock
	public static final String PING_WAKELOCK = TAG + ".client.";

	// mqtt state
	private static final int STATUS_NOT_CONNECTED = 0;
	private static final int STATUS_CONNECTING = 1;
	private static final int STATUS_CONNECTED = 2;
	private static final int STATUS_RECONNECTING = 3;

	public static final int MQTT_QOS_0 = 0; // QOS Level 0 ( Delivery Once no confirmation )
	public static final int MQTT_QOS_1 = 1; // QOS Level 1 ( Delevery at least Once with confirmation )
	public static final int MQTT_QOS_2 = 2; // QOS Level 2 ( Delivery only once with confirmation with handshake )

	private static final boolean MQTT_CLEAN_SESSION = false; // Start a clean session?

	// Retry intervals, when the connection is lost.
	private static final long INITIAL_RETRY_INTERVAL = 1000 * 60;
	private static final long INITIAL_MAX_INTERVAL = 86400000; // Interval MAX 24H

	private SharedPreferences m_Prefs = null;

	private MqttConnectOptions m_Opts = null; // Connection Options

	private MqttAsyncClient m_Client = null;

	private ConnectivityManager m_ConnectivityManager = null; // To check for connectivity changes

	private URI m_ServerFullUri = null;

	private WifiManager.WifiLock m_WifiLock = null;

	private WakeLock m_Cpulock = null;

	private PMSDB m_DB = null;

	private FileUtil m_Futil = null;

	private Timer m_Timer = null;

	// client id
	private String m_strClientID = "";
	private String m_strTopic = "";
	private String m_strServerProtocol = "";
	private String m_strServerIp = "";
	private String m_strAction = "";

	private int m_nServerPort = 0;

	private MQTTService mServer = null;

	public MQTTService() {
		super();
	}

	/**
	 * Start MQTT Client
	 * 
	 * @param Context
	 *        context to start the service with
	 * @return void
	 */
	public static void actionStart (Context ctx) {
		Intent i = new Intent(ctx, MQTTService.class);
		i.setAction(ACTION_START);
		ctx.startService(i);
	}

	/**
	 * Stop MQTT Service
	 * 
	 * @param Context
	 *        context to start the service with
	 * @return void
	 */
	public static void actionStop (Context ctx) {
		Intent i = new Intent(ctx, MQTTService.class);
		i.setAction(ACTION_STOP);
		ctx.startService(i);
	}

	/**
	 * Network Change Start MQTT Service
	 * 
	 * @param Context
	 *        context to start the service with
	 * @return void
	 */
	public static void actionNetworkChangeRestart (Context ctx) {
		Intent i = new Intent(ctx, MQTTService.class);
		i.setAction(ACTION_CHANGERECONNECT);
		ctx.startService(i);
	}

	@Override
	public IBinder onBind (Intent intent) {
		CLog.e(TAG, "onBind()");
		return null;
	}

	@Override
	public void onDestroy () {
		CLog.e(TAG, "onDestroy()");
		try {
			stop();
		} catch (Exception e) {
			CLog.e(e.getMessage());
		}
		super.onDestroy();
	}

	/**
	 * Initalizes the DeviceId and most instance variables Including the Connection Handler, Datastore, Alarm Manager and ConnectivityManager.
	 */
	@Override
	public void onCreate () {
		super.onCreate();

		m_Prefs = getSharedPreferences(PREF_FILE_NAME, MODE_PRIVATE);

		m_Futil = new FileUtil();

		m_Opts = new MqttConnectOptions();
		m_Opts.setCleanSession(MQTT_CLEAN_SESSION);

		m_DB = PMSDB.getInstance(getApplicationContext());

		mServer = this;

		m_ConnectivityManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
	}

	/**
	 * µ Service onStartCommand Handles the action passed via the Intent
	 * 
	 * @return START_REDELIVER_INTENT
	 */
	@Override
	@SuppressLint({ "Override", "InlinedApi" })
	public synchronized int onStartCommand (Intent intent, int flags, int startId) {
		CLog.d(TAG, "onStartCommand:" + flags + "," + startId + "," + intent);

		try {
			if (FLAG_N.equals(PMSUtil.getMQTTFlag(getApplicationContext())) || FLAG_N.equals(PMSUtil.getPrivateFlag(getApplicationContext()))) {
				stop();
				return START_FLAG_REDELIVERY;
			}

			m_ServerFullUri = new URI(PMSUtil.getMQTTServerUrl(getApplicationContext()));

			m_strServerProtocol = m_ServerFullUri.getScheme();
			m_strServerIp = m_ServerFullUri.getHost();
			m_nServerPort = m_ServerFullUri.getPort();
			m_strTopic = PMSUtil.getApplicationKey(getApplicationContext());

			// URL에 대한 유효성 Check
			if ((m_strServerProtocol != null) && (m_strServerIp != null) && (m_nServerPort != -1)) {
				try {
					m_strAction = intent.getAction();
					if (m_strAction == null) {
						CLog.i(TAG, "Starting service with no action Probably from a crash");
						m_strAction = "";
					} else if (m_Client == null) {
						m_Prefs.edit().putInt(PREF_MQTT_STATUS, STATUS_NOT_CONNECTED).commit();
						start();
					} else if (m_strAction.equals(ACTION_FORCE_START)) {
						if (isConnected() == false) {
							m_Prefs.edit().putInt(PREF_MQTT_STATUS, STATUS_NOT_CONNECTED).commit();
							start();
						}
					} else {
						if (m_strAction.equals(ACTION_STOP)) {
							stop();
						} else {
							int state = getCheckConnection();
							if (state == STATUS_NOT_CONNECTED) {
								if (m_strAction.equals(ACTION_START) || m_strAction.equals(ACTION_CHANGERECONNECT)) {
									start();
								}
							} else if (state == STATUS_RECONNECTING) {
								if (m_strAction.equals(ACTION_RECONNECT)) {
									if (isNetworkAvailable()) {
										reconnectIfNecessary();
									}
								}
							}
						}
					}
				} catch (NullPointerException e) {
					CLog.e(TAG, "NullPointerException");
					if (intent == null) {
						m_Prefs.edit().putInt(PREF_MQTT_STATUS, STATUS_NOT_CONNECTED).commit();
						start();
					}
				}
			} else {
				CLog.e(TAG, "AndroidManifast.xml 에 Sever URL이 설정이 되지 않았습니다.");
			}
		} catch (URISyntaxException e1) {
			CLog.e(TAG, "AndroidManifast.xml 에 Sever URL이 설정이 되지 않았습니다.");
		}

		return START_FLAG_REDELIVERY;
	}

	private int getCheckConnection () {
		int status = m_Prefs.getInt(PREF_MQTT_STATUS, STATUS_NOT_CONNECTED);
		switch (status) {
			case STATUS_NOT_CONNECTED:
				CLog.i(TAG, "not connected");
				break;
			case STATUS_CONNECTING:
				CLog.d(TAG, "already request connecting");
				break;
			case STATUS_CONNECTED:
				CLog.d(TAG, "already connected");
				break;
			case STATUS_RECONNECTING:
				CLog.d(TAG, "already recconnecting");
				break;
		}

		return status;
	}

	@Override
	public void onStart (final Intent intent, int startId) {
		CLog.d(TAG, "onStart:" + startId);
	}

	/**
	 * Attempts connect to the Mqtt Broker and listen for Connectivity changes via ConnectivityManager.CONNECTVITIY_ACTION BroadcastReceiver
	 */
	private synchronized void start () {
		CLog.i(TAG, "MQTT Service Start");
		connect();
	}

	/**
	 * Attempts to stop the Mqtt client as well as halting all keep alive messages queued in the alarm manager
	 */
	private synchronized void stop () {
		CLog.i(TAG, "MQTT Service Stop");

		if (ACTION_STOP.equals(m_strAction)) {
			int status = getCheckConnection();
			if (status == STATUS_NOT_CONNECTED || status == STATUS_RECONNECTING) {
				CLog.i(TAG, "Attemtpign to stop connection that isn't running");
				return;
			}
		} else {
			m_Prefs.edit().putInt(PREF_MQTT_STATUS, STATUS_NOT_CONNECTED).commit();
		}

		if (m_Client != null) {
			new Thread(new Runnable() {
				@Override
				public void run () {
					try {
						CLog.e(TAG, "MQTT Disconnect()!!!");
						m_Client.disconnect(null, new IMqttActionListener() {
							@Override
							public void onSuccess (IMqttToken asyncActionToken) {
								CLog.d(TAG, "MQTT Disconnect Succsess");
								try {
									m_Client.close();
									m_Futil.getConnectLogUtil(getApplicationContext(), 6, "");
								} catch (MqttException e) {
									CLog.e(e.getMessage());
								} catch (Exception ex) {
									CLog.e(ex.getMessage());
								} finally {
									m_Client = null;
									m_strAction = "";
									cancelReconnect();
									stopSelf();
								}
							}

							@Override
							public void onFailure (IMqttToken asyncActionToken, Throwable exception) {
								CLog.d(TAG, "MQTT Disconnect Failure " + exception.toString());
								m_Client = null;
								m_strAction = "";
								cancelReconnect();
								stopSelf();
							}
						});
					} catch (MqttException ex) {
						CLog.e(ex.getMessage());
					} catch (Exception e) {
						CLog.e(e.getMessage());
					} finally {
						cancelWakeUpSetting();
					}
				}
			}).start();
		}
	}

	private synchronized void sleepWakeUpSetting () {
		CLog.i(TAG, "sleepWakeUpSetting()");
		WifiManager wifiManager = (WifiManager) getSystemService(WIFI_SERVICE);
		m_WifiLock = wifiManager.createWifiLock("wifilock");
		m_WifiLock.setReferenceCounted(true);

		PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
		m_Cpulock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "Tag");

		if (!pm.isScreenOn()) {
			m_Cpulock.acquire();
			m_WifiLock.acquire();
		}
	}

	private void cancelWakeUpSetting () {
		CLog.i(TAG, "cancelWakeUpSetting()");
		try {
			if (m_WifiLock.isHeld() && (m_WifiLock != null)) {
				m_WifiLock.release();
				m_WifiLock = null;
			}
		} catch (NullPointerException e) {
			m_WifiLock = null;
		}

		try {
			if (m_Cpulock.isHeld() && (m_Cpulock != null)) {
				m_Cpulock.release();
				m_Cpulock = null;
			}
		} catch (NullPointerException e) {
			m_Cpulock = null;
		}
	}

	/**
	 * Connects to the broker with the appropriate datastore
	 */
	private synchronized void connect () {
		new Thread(new Runnable() {
			@Override
			public void run () {
				try {
					CLog.i(TAG, "Connecting with URL: " + m_ServerFullUri.toString() + ", topic = " + m_strTopic);

					insertPrivateLogs(Logs.START);
					m_Prefs.edit().putInt(PREF_MQTT_STATUS, STATUS_CONNECTING).commit();
					while (StringUtil.isEmpty(PMSUtil.getAppUserId(getApplicationContext()))) {
						CLog.i(TAG, "appUserId is null, sleep(5000)");
						try {
							Thread.sleep(5000);
						} catch (Exception e) {
							CLog.e(e.getMessage());
						}
					}

					m_strClientID = PMSUtil.getAppUserId(getApplicationContext());
					CLog.d(TAG, "clientId = " + m_strClientID);

					sleepWakeUpSetting();

					MqttDefaultFilePersistence mdfp = new MqttDefaultFilePersistence(getFilesDir().toString());

                    org.eclipse.paho.client.mqttv3.logging.CLog.setDebugMode(false);

					if (m_Client == null) {
						m_Client = new MqttAsyncClient(m_ServerFullUri.toString(), m_strClientID, mdfp, new AlarmPingSender(mServer));
					}

					final int keepAlive = getKeepAlive();
					m_Opts.setKeepAliveInterval(keepAlive); // 10m
					m_Opts.setConnectionTimeout(30);
					m_Opts.setUserName(m_strClientID);
					m_Opts.setPassword(m_strTopic.toCharArray());
					if ("ssl".equals(m_strServerProtocol)) {
						try {
							String ssl = PMSUtil.getSingKey(getApplicationContext());
							String pass = PMSUtil.getSignPass(getApplicationContext());
							m_Opts.setSocketFactory(SelfSignedSocketFactory.getSSLSocketFactory(new ByteArrayInputStream(ssl.getBytes()), pass));
						} catch (Exception e) {
							e.printStackTrace();
							if (m_Prefs.getLong(PREF_RETRY, 1) == 1) {
								int time = (randomRange(0, 300) * 1000);
								CLog.i("Timer = " + time + "ms");
								m_Timer = new Timer();
								m_Timer.schedule(new UpdateTimeTask(), time);
							} else {
								getSSLSignKey();
							}
							return;
						}
					}

					m_Client.setCallback(MQTTService.this);

					CLog.d(TAG, "try connect to broker");
					m_Client.connect(m_Opts, null, new IMqttActionListener() {
						@Override
						public void onSuccess (IMqttToken asyncActionToken) {
							try {
								if (isAlreadySubscribe() == true) {
									CLog.i(TAG, "Subscrube Call");
									m_Client.subscribe(m_strTopic, MQTT_QOS_2);
								}

								CLog.d(TAG, "succsess connect, topic = " + m_strTopic);
								m_Prefs.edit().putLong(PREF_RETRY, 1).commit();
								try {
									insertPrivateLogs(Logs.SUCCSESS);
									m_Futil.getConnectLogUtil(getApplicationContext(), 1, m_strClientID, String.valueOf(keepAlive));
								} catch (Exception e) {
									CLog.e(e.getMessage());
								}

								m_Prefs.edit().putInt(PREF_MQTT_STATUS, STATUS_CONNECTED).commit();
							} catch (MqttException e) {
								CLog.e(e.getMessage());
							} catch (Exception e) {
								CLog.e(e.getMessage());
							} finally {
								cancelWakeUpSetting();
							}
						}

						@Override
						public void onFailure (IMqttToken asyncActionToken, Throwable exception) {
							CLog.e(TAG, "Failure connect!! " + exception.toString());
							String ex = exception.toString();
							if (ex.indexOf("SSLHandshake") != -1) {
								if (m_Prefs.getLong(PREF_RETRY, 1) == 1) {
									int time = (randomRange(0, 300) * 1000);
									CLog.e("Timer = " + time + "ms");
									m_Timer = new Timer();
									m_Timer.schedule(new UpdateTimeTask(), time);
								} else {
									getSSLSignKey();
								}
							} else {
								m_Prefs.edit().putInt(PREF_MQTT_STATUS, STATUS_NOT_CONNECTED).commit();
								insertPrivateLogs(Logs.FAIL);
								scheduleReconnect();
								cancelWakeUpSetting();
							}
						}
					});
				} catch (MqttSecurityException e) {
					CLog.e(TAG, "MqttSecurityException: " + (e.getMessage() != null ? e.getMessage() : "NULL"));
					insertPrivateLogs(Logs.FAIL);
					scheduleReconnect();
				} catch (MqttException e) {
					CLog.e(TAG, "MqttException: " + (e.getMessage() != null ? e.getMessage() : "NULL"));
					insertPrivateLogs(Logs.FAIL);
					scheduleReconnect();
				} finally {
					cancelWakeUpSetting();
				}
			}
		}).start();
	}

	private boolean isAlreadySubscribe () {
		if (m_Prefs.getBoolean(PREF_APP_FIRST, false) == false) {
			m_Prefs.edit().putBoolean(PREF_APP_FIRST, true).commit();
			return true;
		}

		return false;
	}

	private void getSSLSignKey () {
		new GetSignKey(getApplicationContext()).request(m_strTopic, new APICallback() {
			@Override
			public void response (String code, JSONObject json) {
				m_Prefs.edit().putInt(PREF_MQTT_STATUS, STATUS_NOT_CONNECTED).commit();
				insertPrivateLogs(Logs.FAIL);
				cancelWakeUpSetting();
				scheduleReconnect();
				if (m_Timer != null) {
					m_Timer.cancel();
					m_Timer = null;
				}
			}
		});
	}

	private int getKeepAlive () {
		int startTime = randomRange(0, 60); // 2m ~ 4m 사이 랜덤값
		int mataKeepAlive = Integer.valueOf(PMSUtil.getMQTTServerKeepAlive(getApplicationContext()));
		int keepAlive = startTime + mataKeepAlive;
		m_Prefs.edit().putInt(PREF_KEEPALIVE_INTERVAL, keepAlive).commit();

		CLog.i(TAG, "Keep Alive Time -> " + keepAlive + "s");
		return keepAlive;
	}

	private int randomRange (int n1, int n2) {
		return (int) (Math.random() * (n2 - n1 + 1)) + n1;
	}

	// We schedule a reconnect based on the starttime of the service
	public void scheduleReconnect () {
		CLog.i(TAG, "scheduleReconnect");

		int StartTime = randomRange(0, 60000);
		long now = System.currentTimeMillis();
		long interval_Count = m_Prefs.getLong(PREF_RETRY, 1);
		long interval = ((int) (INITIAL_RETRY_INTERVAL) * interval_Count) + StartTime;

		CLog.i(TAG, "State Time -> " + StartTime + "ms, Interval -> " + interval + "ms, Interval Count -> " + interval_Count);

		if (INITIAL_MAX_INTERVAL <= interval) {
			interval_Count = 1;
			interval = ((int) (INITIAL_RETRY_INTERVAL) * interval_Count) + StartTime;
		}

		CLog.i(TAG, "Rescheduling connection in " + interval + "ms");
		try {
			m_Futil.getConnectLogUtil(getApplicationContext(), 3, interval, interval_Count);
		} catch (Exception e) {
			CLog.e(e.getMessage());
		}

		m_Prefs.edit().putLong(PREF_RETRY, interval_Count * 2).commit();
		m_Prefs.edit().putInt(PREF_MQTT_STATUS, STATUS_RECONNECTING).commit();

		Intent i = new Intent();
		i.setClass(this, MQTTService.class);
		i.setAction(ACTION_RECONNECT);
		PendingIntent pi = PendingIntent.getService(this, 0, i, 0);
		AlarmManager alarmMgr = (AlarmManager) getSystemService(ALARM_SERVICE);
		alarmMgr.set(AlarmManager.RTC_WAKEUP, (now + interval), pi);
	}

	// Remove the scheduled reconnect
	public void cancelReconnect () {
		CLog.i(TAG, "cancelReconnect");

		Intent i = new Intent();
		i.setClass(this, MQTTService.class);
		i.setAction(ACTION_RECONNECT);
		PendingIntent pi = PendingIntent.getService(this, 0, i, 0);
		AlarmManager alarmMgr = (AlarmManager) getSystemService(ALARM_SERVICE);
		alarmMgr.cancel(pi);
	}

	/**
	 * Checkes the current connectivity and reconnects if it is required.
	 */
	private synchronized void reconnectIfNecessary () {
		int status = getCheckConnection();
		CLog.i(TAG, "reconnectIfNecessary status -> " + status + ", m_Client -> " + m_Client);
		if ((status == STATUS_RECONNECTING) && (isConnected() == false)) {
			connect();
		}
	}

	/**
	 * Query's the NetworkInfo via ConnectivityManager to return the current connected state
	 * 
	 * @return boolean true if we are connected false otherwise
	 */
	private boolean isNetworkAvailable () {
		NetworkInfo info = m_ConnectivityManager.getActiveNetworkInfo();
		return (info == null) ? false : info.isConnected();
	}

	/**
	 * Verifies the client State with our local connected state
	 * 
	 * @return true if its a match we are connected false if we aren't connected
	 */
	private boolean isConnected () {
		if (m_Client != null && !m_Client.isConnected()) {
			CLog.i(TAG, "Mismatch between what we think is connected and what is connected");
		}

		if (m_Client != null) {
			return (m_Client.isConnected()) ? true : false;
		}

		return false;
	}

	/**
	 * Connectivity Lost from broker
	 */
	@Override
	public void connectionLost (Throwable arg0) {
		CLog.i(TAG, "connectionLost() -> " + arg0.toString());
		try {
			if (isNetworkAvailable()) {
				m_Prefs.edit().putInt(PREF_MQTT_STATUS, STATUS_NOT_CONNECTED).commit();
			} else {
				stop();
			}

			insertPrivateLogs(Logs.STOP);
			m_Futil.getConnectLogUtil(this, 2, "");

			if (isNetworkAvailable() && (getCheckConnection() == STATUS_NOT_CONNECTED)) {
				m_Prefs.edit().putInt(PREF_MQTT_STATUS, STATUS_RECONNECTING).commit();
				reconnectIfNecessary();
			}
		} catch (Exception e) {
			CLog.e(e.getMessage());
		}
	}

	public void insertPrivateLogs (String flag) {
		Logs logs = new Logs();
		logs.date = DateUtil.getNowDateMo();
		logs.time = DateUtil.getNowTime();
		logs.logFlag = Logs.TYPE_P;
		logs.privateLog = flag;

		if (FLAG_Y.equals(PMSUtil.getPrivateLogFlag(getApplicationContext()))) {
			m_DB.insertLog(logs);
		}
	}

	/**
	 * Publish Message Completion
	 */
	@Override
	public void deliveryComplete (IMqttDeliveryToken arg0) {
	}

	/**
	 * Received Message from broker
	 */
	@Override
	public void messageArrived (String mqttTopic, MqttMessage mqttMessage) throws Exception {
		String message = new String(mqttMessage.getPayload());
		CLog.d(TAG, "messageArrived:message = " + message);
		broadcastReceivedMessage(message);
	}

	/**
	 * broadcast received message
	 * 
	 * @param message
	 */
	private void broadcastReceivedMessage (String message) {
		CLog.d(TAG, "broadcastReceivedMessage:message = " + message);

		Intent broadcastIntent = new Intent(INTENT_RECEIVED_MSG);
		broadcastIntent.addCategory(getApplication().getPackageName());

		broadcastIntent.putExtra(KEY_MSG, message);
		sendBroadcast(broadcastIntent);
	}

	class UpdateTimeTask extends TimerTask {
		@Override
		public void run () {
			CLog.i("UpdateTimeTask!!!");
			getSSLSignKey();
		}
	}
}