package com.pms.sdk.push.mqtt;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.pms.sdk.IPMSConsts;
import com.pms.sdk.common.util.CLog;
import com.pms.sdk.common.util.FileUtil;

/**
 * Connection Change Receiver
 * 
 * @author erzisk
 * @since 2013.07.23
 */
public class ConnectionChangeReceiver extends BroadcastReceiver implements IPMSConsts {

	private static final String TAG = "MQTT";

	@SuppressWarnings("deprecation")
	@Override
	public void onReceive (Context context, Intent intent) {
		CLog.i(TAG, "onReceive");

		NetworkInfo info = (NetworkInfo) intent.getParcelableExtra(ConnectivityManager.EXTRA_NETWORK_INFO);
		boolean hasConnectivity = (info != null && info.isConnected()) ? true : false;

		CLog.e(TAG, "Connectivity changed: connected = " + hasConnectivity);

		FileUtil fileUtil = new FileUtil();
		try {
			fileUtil.getConnectLogUtil(context, 4, hasConnectivity);
		} catch (Exception e) {
			CLog.e(e.getMessage());
		}

		if (hasConnectivity) {
			Intent i = new Intent(context, RestartReceiver.class);
			i.setAction(ACTION_CHANGERECONNECT);
			context.sendBroadcast(i);
		} /*
		 * else { Intent i = new Intent(context, RestartReceiver.class); i.setAction(ACTION_STOP); context.sendBroadcast(i); context.stopService(new
		 * Intent(context, MQTTService.class)); }
		 */
	}
}
