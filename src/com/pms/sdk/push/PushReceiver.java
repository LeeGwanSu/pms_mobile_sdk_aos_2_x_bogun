package com.pms.sdk.push;

import java.lang.reflect.Field;
import java.util.List;

import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.support.v4.app.NotificationCompat;
import android.widget.RemoteViews;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.ImageLoader.ImageContainer;
import com.android.volley.toolbox.ImageLoader.ImageListener;
import com.pms.sdk.IPMSConsts;
import com.pms.sdk.PMS;
import com.pms.sdk.api.QueueManager;
import com.pms.sdk.bean.Msg;
import com.pms.sdk.bean.PushMsg;
import com.pms.sdk.common.util.CLog;
import com.pms.sdk.common.util.DateUtil;
import com.pms.sdk.common.util.PMSUtil;
import com.pms.sdk.common.util.PhoneState;
import com.pms.sdk.common.util.Prefs;
import com.pms.sdk.common.util.StringUtil;
import com.pms.sdk.db.PMSDB;
import com.pms.sdk.push.mqtt.MQTTService;
import com.pms.sdk.view.BitmapLruCache;

/**
 * push receiver
 * 
 * @author erzisk
 * @since 2013.06.07
 */
public class PushReceiver extends BroadcastReceiver implements IPMSConsts {

	private static final int START_TASK_TO_FRONT = 2;

	private static final String USE = "Y";

	// notification id
	public final static int NOTIFICATION_ID = 0x253470;
	private Prefs mPrefs;

	private PowerManager pm;
	private PowerManager.WakeLock wl;

	private static final int DEFAULT_SHOWING_TIME = 30000;

	private final Handler mFinishHandler = new Handler();

	private Bitmap mPushImage;

	/**
	 * gcm key register
	 * 
	 * @param context
	 * @param senderId
	 */
	public static void gcmRegister (Context context, String senderId) {
		Intent registrationIntent = new Intent(ACTION_REGISTER);
		registrationIntent.setPackage(GSF_PACKAGE);
		registrationIntent.putExtra(KEY_APP, PendingIntent.getBroadcast(context, 0, new Intent(), 0));
		registrationIntent.putExtra(KEY_SENDER, senderId);
		context.startService(registrationIntent);
	}

	@Override
	public synchronized void onReceive (final Context context, final Intent intent) {
		CLog.i("onReceive() -> " + intent.toString());
		mPrefs = new Prefs(context);

		if (intent.getAction().equals(ACTION_REGISTRATION)) {
			// registration
			CLog.i("onReceive:registration");
			if (intent.getStringExtra(KEY_GCM_TOKEN) != null) {
				// regist gcm key
				CLog.d("handleRegistration:key=" + intent.getStringExtra(KEY_GCM_TOKEN));
				mPrefs.putString(KEY_GCM_TOKEN, intent.getStringExtra(KEY_GCM_TOKEN));
			} else {
				// error occurred
				CLog.i("handleRegistration:error=" + intent.getStringExtra("error"));
				CLog.i("handleRegistration:unregistered=" + intent.getStringExtra("unregistered"));
			}
		} else {
			// receive push message
			if (intent.getAction().equals(MQTTService.INTENT_RECEIVED_MSG)) {
				// private server
				String message = intent.getStringExtra(MQTTService.KEY_MSG);

				CLog.i("onReceive:receive from private server");

				// set push info
				try {
					JSONObject msgObj = new JSONObject(message);
					if (msgObj.has(KEY_MSG_ID)) {
						intent.putExtra(KEY_MSG_ID, msgObj.getString(KEY_MSG_ID));
					}
					if (msgObj.has(KEY_NOTI_TITLE)) {
						intent.putExtra(KEY_NOTI_TITLE, msgObj.getString(KEY_NOTI_TITLE));
					}
					if (msgObj.has(KEY_MSG_TYPE)) {
						intent.putExtra(KEY_MSG_TYPE, msgObj.getString(KEY_MSG_TYPE));
					}
					if (msgObj.has(KEY_NOTI_MSG)) {
						intent.putExtra(KEY_NOTI_MSG, msgObj.getString(KEY_NOTI_MSG));
					}
					if (msgObj.has(KEY_MSG)) {
						intent.putExtra(KEY_MSG, msgObj.getString(KEY_MSG));
					}
					if (msgObj.has(KEY_SOUND)) {
						intent.putExtra(KEY_SOUND, msgObj.getString(KEY_SOUND));
					}
					if (msgObj.has(KEY_NOTI_IMG)) {
						intent.putExtra(KEY_NOTI_IMG, msgObj.getString(KEY_NOTI_IMG));
					}
					if (msgObj.has(KEY_COLOR_FLAG)) {
						intent.putExtra(KEY_COLOR_FLAG, msgObj.getString(KEY_COLOR_FLAG));
					}
					if (msgObj.has(KEY_TITLE_COLOR)) {
						intent.putExtra(KEY_TITLE_COLOR, msgObj.getString(KEY_TITLE_COLOR));
					}
					if (msgObj.has(KEY_CONTENT_COLOR)) {
						intent.putExtra(KEY_CONTENT_COLOR, msgObj.getString(KEY_CONTENT_COLOR));
					}
					if (msgObj.has(KEY_NOT_POPUP)) {
						intent.putExtra(KEY_NOT_POPUP, msgObj.getString(KEY_NOT_POPUP));
					}
					if (msgObj.has(KEY_DATA)) {
						intent.putExtra(KEY_DATA, msgObj.getString(KEY_DATA));
					}
				} catch (Exception e) {
					CLog.e(e.getMessage());
				}
				onPushMessage(context, intent);
			} else if (intent.getAction().equals(ACTION_RECEIVE)) {
				context.sendBroadcast(new Intent(ACTION_FORCE_START).putExtras(intent));
				// gcm
				// key: title, msgType, message, sound, data
				CLog.i("onReceive:receive from GCM");
				onPushMessage(context, intent);
			}
		}
	}

	private synchronized void onPushMessage (final Context context, final Intent intent) {
		String firstTime = PMSUtil.getFirstTime(context);
		String secondTime = PMSUtil.getSecondTime(context);
		Boolean pushCancel = true;

		CLog.i("Allowed Time Flag : " + PMSUtil.getAllowedTime(context));

		if (PMSUtil.getAllowedTime(context)) {
			if ((StringUtil.isEmpty(firstTime) == false) && (StringUtil.isEmpty(secondTime) == false)) {
				pushCancel = DateUtil.getCompareDate(firstTime, secondTime);
			}
		}

		CLog.i("Auto Push Cancel : " + pushCancel);
		if (pushCancel) {
			if (isImagePush(intent.getExtras())) {
				// image push
				QueueManager queueManager = QueueManager.getInstance();
				RequestQueue queue = queueManager.getRequestQueue();
				ImageLoader imageLoader = new ImageLoader(queue, new BitmapLruCache());
				imageLoader.get(intent.getStringExtra(KEY_NOTI_IMG), new ImageListener() {
					@Override
					public void onResponse (ImageContainer response, boolean isImmediate) {
						if (response == null) {
							CLog.e("response is null");
							return;
						}
						if (response.getBitmap() == null) {
							CLog.e("bitmap is null");
							return;
						}
						mPushImage = response.getBitmap();
						CLog.i("imageWidth:" + mPushImage.getWidth());
						onMessage(context, intent);
					}

					@Override
					public void onErrorResponse (VolleyError error) {
						CLog.e("onErrorResponse:" + error.getMessage());
						// wrong img url (or exception)
						onMessage(context, intent);
					}
				});

			} else {
				// default push
				onMessage(context, intent);
			}
		}
	}

	/**
	 * on message (gcm, private msg receiver)
	 * 
	 * @param context
	 * @param intent
	 */
	private synchronized void onMessage (final Context context, Intent intent) {

		final Bundle extras = intent.getExtras();

		PMS pms = PMS.getInstance(context);

		PushMsg pushMsg = new PushMsg(extras);

		if (StringUtil.isEmpty(pushMsg.msgId) || StringUtil.isEmpty(pushMsg.notiTitle) || StringUtil.isEmpty(pushMsg.notiMsg)
				|| StringUtil.isEmpty(pushMsg.msgType)) {
			CLog.i("msgId or notiTitle or notiMsg or msgType is null");
			return;
		}

		CLog.i(pushMsg + "");

		PMSDB db = PMSDB.getInstance(context);

		// check already exist msg
		Msg existMsg = db.selectMsgWhereMsgId(pushMsg.msgId);
		if (existMsg != null && existMsg.msgId.equals(pushMsg.msgId)) {
			CLog.i("already exist msg");
			return;
		}

		// insert (temp) new msg
		Msg newMsg = new Msg();
		newMsg.readYn = Msg.READ_N;
		newMsg.msgGrpCd = "999999";
		newMsg.expireDate = "0";
		newMsg.msgId = pushMsg.msgId;

		db.insertMsg(newMsg);

		// refresh list and badge
		context.sendBroadcast(new Intent(RECEIVER_PUSH).putExtras(extras));

		// show noti
		// ** 수정 ** 기존 코드는 PREF_MSG_FLAG로 되어 있어 NOTI_FLAG로 변경 함
		CLog.i("NOTI FLAG : " + mPrefs.getString(PREF_NOTI_FLAG));

		if (USE.equals(mPrefs.getString(PREF_NOTI_FLAG)) || StringUtil.isEmpty(mPrefs.getString(PREF_NOTI_FLAG))) {

			// check push flag
			if (USE.equals(mPrefs.getString(IPMSConsts.PREF_SCREEN_WAKEUP_FLAG)) || StringUtil.isEmpty(mPrefs.getString(PREF_SCREEN_WAKEUP_FLAG))) {
				// screen on
				pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
				wl = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, "Tag");
				if (!pm.isScreenOn()) {
					wl.acquire();
					mFinishHandler.postDelayed(finishRunnable, DEFAULT_SHOWING_TIME);
				}
			}
			CLog.i("version code :" + Build.VERSION.SDK_INT);

			// execute push noti listener
			if (pms.getOnReceivePushListener() != null) {
				if (pms.getOnReceivePushListener().onReceive(context, extras)) {
					showNotification(context, extras);
				}
			} else {
				showNotification(context, extras);
			}

			CLog.i("ALERT FLAG : " + mPrefs.getString(PREF_ALERT_FLAG));
			CLog.i("NOT POPUP FLAG : " + pushMsg.notPopup);

			if ((USE.equals(pushMsg.notPopup) == false) && (USE.equals(mPrefs.getString(PREF_ALERT_FLAG)))) {
				showPopup(context, extras);
			}
		}
	}

	/**
	 * show notification
	 * 
	 * @param context
	 * @param extras
	 */
	private synchronized void showNotification (Context context, Bundle extras) {

		// push intent sample
		// String msgId = extras.getString(KEY_MSG_ID);
		// String title = extras.getString(KEY_TITLE);
		// String msg = extras.getString(KEY_MSG);
		// String msgType = extras.getString(KEY_MSG_TYPE);
		// String sound = extras.getString(KEY_SOUND);
		// String data = extras.getString(KEY_DATA);
		CLog.i("showNotification");
		if (isImagePush(extras)) {
			showNotificationImageStyle(context, extras);
		} else {
			showNotificationTextStyle(context, extras);
		}
	}

	/**
	 * show notification text style
	 * 
	 * @param context
	 * @param extras
	 */
	private synchronized void showNotificationTextStyle (Context context, Bundle extras) {
		CLog.i("showNotificationTextStyle");
		// push info
		PushMsg pushMsg = new PushMsg(extras);

		// notification
		int iconId = PMSUtil.getIconId(context);
		int largeIconId = mPrefs.getInt(PREF_LARGE_NOTI_ICON) > 0 ? mPrefs.getInt(PREF_LARGE_NOTI_ICON) : 0;

		NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
		builder.setNumber(PMSDB.getInstance(context).selectNewMsgCnt());
		builder.setContentIntent(makePendingIntent(context, extras));
		builder.setAutoCancel(true);
		builder.setContentText(pushMsg.notiMsg);
		builder.setContentTitle(pushMsg.notiTitle);
		builder.setTicker(pushMsg.notiMsg);
		// setting lights color
		builder.setLights(Notification.FLAG_SHOW_LIGHTS, 1000, 2000);

		// set small icon
		CLog.i("small icon :" + iconId);
		if (iconId > 0) {
			builder.setSmallIcon(iconId);
		}

		// set large icon
		CLog.i("large icon :" + largeIconId);
		if (largeIconId > 0) {
			builder.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), largeIconId));
		}

		// setting ring mode
		int noti_mode = ((AudioManager) context.getSystemService(Context.AUDIO_SERVICE)).getRingerMode();
		switch (noti_mode) {
			case AudioManager.RINGER_MODE_NORMAL:
				if (FLAG_Y.equals(mPrefs.getString(PREF_RING_FLAG)) || StringUtil.isEmpty(mPrefs.getString(PREF_RING_FLAG))) {
					try {
						int notiSound = mPrefs.getInt(PREF_NOTI_SOUND);
						if (notiSound > 0) {
							Uri soundUri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + context.getPackageName() + "/" + notiSound);
							builder.setSound(soundUri);
						} else {
							throw new Exception("DEFAULT_SOUND");
						}
					} catch (Exception e) {
						Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
						builder.setSound(uri);
					}
				}
				break;

			case AudioManager.RINGER_MODE_VIBRATE:
				if (FLAG_Y.equals(mPrefs.getString(PREF_VIBE_FLAG)) || StringUtil.isEmpty(mPrefs.getString(PREF_VIBE_FLAG))) {
					builder.setDefaults(Notification.DEFAULT_VIBRATE);
				}
				break;

			case AudioManager.RINGER_MODE_SILENT:
				break;
		}

		int SDK_INT = Build.VERSION.SDK_INT;
		if (PMSUtil.getIsCustNoti(context) && (FLAG_Y.equals(pushMsg.colorFlag)) && (SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)) {
			RemoteViews cutmNoti = new RemoteViews(context.getApplicationContext().getPackageName(), PMSUtil.getNotiResId(context));
			builder.setContent(cutmNoti);
			cutmNoti.setTextViewText(PMSUtil.getNotiTitleResId(context), pushMsg.notiTitle);
			cutmNoti.setTextColor(PMSUtil.getNotiTitleResId(context), Color.parseColor(pushMsg.titleColor));
			cutmNoti.setTextViewText(PMSUtil.getNotiContenResId(context), pushMsg.notiMsg);
			cutmNoti.setTextColor(PMSUtil.getNotiContenResId(context), Color.parseColor(pushMsg.contentColor));
		}

		// show notification
		NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		notificationManager.notify(NOTIFICATION_ID, builder.build());
	}

	/**
	 * show notification image style
	 * 
	 * @param context
	 * @param extras
	 */
	@SuppressLint("NewApi")
	private synchronized void showNotificationImageStyle (Context context, Bundle extras) {
		CLog.i("showNotificationImageStyle");
		// push info
		PushMsg pushMsg = new PushMsg(extras);

		// notification
		int iconId = PMSUtil.getIconId(context);
		int largeIconId = mPrefs.getInt(PREF_LARGE_NOTI_ICON) > 0 ? mPrefs.getInt(PREF_LARGE_NOTI_ICON) : 0;

		Notification.Builder builder = new Notification.Builder(context);
		builder.setNumber(PMSDB.getInstance(context).selectNewMsgCnt());
		builder.setContentIntent(makePendingIntent(context, extras));
		builder.setAutoCancel(true);
		builder.setContentText(PMSUtil.getBigNotiContextMsg(context));
		builder.setContentTitle(pushMsg.notiTitle);
		builder.setTicker(pushMsg.notiMsg);
		// setting lights color
		builder.setLights(0xFF00FF00, 1000, 2000);

		// set small icon
		CLog.i("small icon :" + iconId);
		if (iconId > 0) {
			builder.setSmallIcon(iconId);
		}

		// set large icon
		CLog.i("large icon :" + largeIconId);
		if (largeIconId > 0) {
			builder.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), largeIconId));
		}

		// setting ring mode
		int noti_mode = ((AudioManager) context.getSystemService(Context.AUDIO_SERVICE)).getRingerMode();
		switch (noti_mode) {
			case AudioManager.RINGER_MODE_NORMAL:
				if (FLAG_Y.equals(mPrefs.getString(PREF_RING_FLAG)) || StringUtil.isEmpty(mPrefs.getString(PREF_RING_FLAG))) {
					try {
						int notiSound = mPrefs.getInt(PREF_NOTI_SOUND);
						if (notiSound > 0) {
							Uri soundUri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + context.getPackageName() + "/" + notiSound);
							builder.setSound(soundUri);
						} else {
							throw new Exception("DEFAULT_SOUND");
						}
					} catch (Exception e) {
						Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
						builder.setSound(uri);
					}
				}
				break;

			case AudioManager.RINGER_MODE_VIBRATE:
				if (FLAG_Y.equals(mPrefs.getString(PREF_VIBE_FLAG)) || StringUtil.isEmpty(mPrefs.getString(PREF_VIBE_FLAG))) {
					builder.setDefaults(Notification.DEFAULT_VIBRATE);
				}
				break;

			case AudioManager.RINGER_MODE_SILENT:
				break;
		}

		if (mPushImage == null) {
			CLog.e("mPushImage is null");
		}

		// show notification
		NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		Notification notification = new Notification.BigPictureStyle(builder).bigPicture(mPushImage).setBigContentTitle(pushMsg.notiTitle)
				.setSummaryText(pushMsg.notiMsg).build();
		notificationManager.notify(NOTIFICATION_ID, notification);
	}

	/**
	 * make pending intent
	 * 
	 * @param context
	 * @param extras
	 * @return
	 */
	private PendingIntent makePendingIntent (Context context, Bundle extras) {
		// notification
		String receiverClass = mPrefs.getString(PREF_NOTI_RECEIVER);
		receiverClass = receiverClass != null ? receiverClass : "com.pms.sdk.notification";

		// setting push info to intent
		Intent innerIntent = new Intent(receiverClass).putExtras(extras);

		return PendingIntent.getBroadcast(context, 0, innerIntent, PendingIntent.FLAG_UPDATE_CURRENT);
	}

	/**
	 * show popup (activity)
	 * 
	 * @param context
	 * @param extras
	 */
	@SuppressLint("DefaultLocale")
	private synchronized void showPopup (Context context, Bundle extras) {

		PushMsg pushMsg = new PushMsg(extras);

		if (PMSUtil.getNotiOrPopup(context) && pushMsg.msgType.equals("T")) {
			Toast.makeText(context, pushMsg.notiMsg, Toast.LENGTH_SHORT).show();
		} else {
			Class<?> pushPopupActivity;

			String pushPopupActivityName = mPrefs.getString(PREF_PUSH_POPUP_ACTIVITY);

			if (StringUtil.isEmpty(pushPopupActivityName)) {
				pushPopupActivityName = DEFAULT_PUSH_POPUP_ACTIVITY;
			}

			try {
				pushPopupActivity = Class.forName(pushPopupActivityName);
			} catch (ClassNotFoundException e) {
				CLog.e(e.getMessage());
				pushPopupActivity = PushPopupActivity.class;
			}
			CLog.i("pushPopupActivity :" + pushPopupActivityName);

			Intent pushIntent = new Intent(context, pushPopupActivity);
			pushIntent.setFlags(Intent.FLAG_ACTIVITY_MULTIPLE_TASK | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_ANIMATION
					| Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_NO_HISTORY);
			pushIntent.putExtras(extras);

			if (isOtherApp(context)) {
				context.startActivity(pushIntent);
			}
		}
	}

	@SuppressWarnings({ "deprecation", "static-access" })
	@SuppressLint("DefaultLocale")
	private boolean isOtherApp (Context context) {
		ActivityManager am = (ActivityManager) context.getSystemService(context.ACTIVITY_SERVICE);
		String topActivity = "";

		if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
			RunningAppProcessInfo currentInfo = null;
			Field field = null;
			try {
				field = RunningAppProcessInfo.class.getDeclaredField("processState");
			} catch (NoSuchFieldException e) {
				e.printStackTrace();
			}

			List<RunningAppProcessInfo> appList = am.getRunningAppProcesses();
			for (RunningAppProcessInfo app : appList) {
				if (app.importance == RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
					Integer state = null;
					try {
						state = field.getInt(app);
					} catch (IllegalAccessException e) {
						e.printStackTrace();
					} catch (IllegalArgumentException e) {
						e.printStackTrace();
					}
					if (state != null && state == START_TASK_TO_FRONT) {
						currentInfo = app;
						break;
					}
				}
			}
			topActivity = currentInfo.processName;
		} else {
			List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(10);
			topActivity = taskInfo.get(0).topActivity.getPackageName();
		}

		CLog.e("TOP Activity : " + topActivity);
		if (topActivity.equals(context.getPackageName())) {
			return true;
		}
		return false;
	}

	/**
	 * is image push
	 * 
	 * @param extras
	 * @return
	 */
	private boolean isImagePush (Bundle extras) {
		try {
			if (!PhoneState.isNotificationNewStyle()) {
				throw new Exception("wrong os version");
			}
			String notiImg = extras.getString(KEY_NOTI_IMG);
			CLog.i("notiImg:" + notiImg);
			if (notiImg == null || "".equals(notiImg)) {
				throw new Exception("no image type");
			}
			return true;
		} catch (Exception e) {
			CLog.e("isImagePush:" + e.getMessage());
			return false;
		}
	}

	/**
	 * finish runnable
	 */
	private final Runnable finishRunnable = new Runnable() {
		@Override
		public void run () {
			if (wl != null && wl.isHeld()) {
				wl.release();
			}
		};
	};
}
