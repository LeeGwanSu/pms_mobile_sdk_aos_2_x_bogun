package com.pms.sdk;

public interface IPMSConsts {

	public static final String PMS_VERSION = "2.1";

	// [start] GCM
	public static final String KEY_APP = "app";
	public static final String KEY_SENDER = "sender";
	public static final String KEY_GCM_PROJECT_ID = "gcm_project_id";
	public static final String KEY_GCM_TOKEN = "registration_id";

	public static final String URL_GOOGLE_CLIENT = "https://www.google.com/accounts/ClientLogin";
	public static final String URL_GOOGLE_SENDER = "https://android.apis.google.com/c2dm/send";

	public static final String ACTION_REGISTER = "com.google.android.c2dm.intent.REGISTER";
	public static final String ACTION_REGISTRATION = "com.google.android.c2dm.intent.REGISTRATION";
	public static final String ACTION_RECEIVE = "com.google.android.c2dm.intent.RECEIVE";
	public static final String GSF_PACKAGE = "com.google.android.gsf";

	public static final String RECEIVE_PERMISSION = "com.google.android.c2dm.permission.SEND";

	public static final String GCMRECIVER_PATH = "com.pms.sdk.push.gcm.GCMReceiver";
	// [end] GCM

	// [start] MQTT
	public static final String ACTION_START = "MQTT.START";
	public static final String ACTION_FORCE_START = "MQTT.FORCE_START";
	public static final String ACTION_STOP = "MQTT.STOP";
	public static final String ACTION_RECONNECT = "MQTT.RECONNECT";
	public static final String ACTION_CHANGERECONNECT = "MQTT.CHANGERECONNECT";
	public static final String ACTION_USER_PRESENT = "android.intent.action.USER_PRESENT";
	public static final String ACTION_BOOT_COMPLETED = "android.intent.action.BOOT_COMPLETED";
	public static final String ACTION_ACTION_PACKAGE_RESTARTED = "android.intent.action.ACTION_PACKAGE_RESTARTED";
	// [end] MQTT

	// [start] PUSH
	public static final String KEY_MSG_ID = "i";
	public static final String KEY_NOTI_TITLE = "notiTitle";
	public static final String KEY_NOTI_MSG = "notiMsg";
	public static final String KEY_NOTI_IMG = "notiImg";
	public static final String KEY_MSG = "message";
	public static final String KEY_SOUND = "sound";
	public static final String KEY_MSG_TYPE = "t";
	public static final String KEY_DATA = "d";
	public static final String KEY_COLOR_FLAG = "cF";
	public static final String KEY_TITLE_COLOR = "tC";
	public static final String KEY_CONTENT_COLOR = "cC";
	public static final String KEY_NOT_POPUP = "a";
	// [end] PUSH

	// [start] Receiver
	public static final String RECEIVER_PUSH = "com.pms.sdk.push";
	public static final String RECEIVER_REQUERY = "com.pms.sdk.requery";
	// public static final String RECEIVER_REFRESH_MSG_GRP = "com.pms.sdk.refreshmsggrp";
	// public static final String RECEIVER_REFRESH_MSG = "com.pms.sdk.refreshmsg";
	// public static final String RECEIVER_REFRESH_BADGE = "com.pms.sdk.refreshbadge";
	public static final String RECEIVER_CLOSE_INBOX = "com.pms.sdk.closeinbox";
	public static final String RECEIVER_NOTIFICATION = "com.pms.sdk.notification";
	public static final String DEFAULT_PUSH_POPUP_ACTIVITY = "com.pms.sdk.push.PushPopupActivity";
	public static final String RECEIVER_CHANGE_POPUP = "receiver_popup_change";
	// [end] Receiver

	// [start] Preferences
	public static final String PREF_FILE_NAME = "pref_pms";

	public static final String FLAG_Y = "Y";
	public static final String FLAG_N = "N";
	public static final String PROTOCOL_SSL = "S";
	public static final String PROTOCOL_TCP = "T";

	public static final String PREF_CUST_ID = "cust_id";
	public static final String PREF_LOGINED_CUST_ID = "logined_cust_id";
	public static final String PREF_MAX_USER_MSG_ID = "max_user_msg_id";
	public static final String PREF_SSL_SIGN_KEY = "ssl_sign_key";
	public static final String PREF_SSL_SIGN_PASS = "ssl_sign_pass";

	public static final String PREF_MQTT_FLAG = "mqtt_flag";

	public static final String PREF_CLICK_LIST = "click_list";
	public static final String PREF_READ_LIST = "read_list";

	public static final String PREF_MSG_FLAG = "msg_flag";
	public static final String PREF_NOTI_FLAG = "noti_flag";
	public static final String PREF_API_LOG_FLAG = "api_log_flag";
	public static final String PREF_PRIVATE_LOG_FLAG = "private_log_flag";
	public static final String PREF_PRIVATE_FLAG = "private_flag";
	public static final String PREF_ALERT_FLAG = "alert_flag";
	public static final String PREF_RING_FLAG = "ring_flag";
	public static final String PREF_VIBE_FLAG = "vibe_flag";
	public static final String PREF_ALLOWED_FLAG = "allowed_flag";
	public static final String PREF_SCREEN_WAKEUP_FLAG = "screen_wakeup_flag";
	public static final String PREF_PRIVATE_PROTOCOL = "private_protocol";
	public static final String PREF_ISPOPUP_ACTIVITY = "ispopup_activity";
	public static final String PREF_NOTIORPOPUP_SETTING = "notiorpopup_setting";
	public static final String PREF_IS_CUST_NOTI = "is_cust_noti";

	public static final String PREF_INBOX_ACTIVITY = "pref_inbox_activity";
	public static final String PREF_PUSH_POPUP_ACTIVITY = "pref_push_popup_activity";
	public static final String PREF_PUSH_POPUP_SHOWING_TIME = "pref_push_popup_showing_time";

	public static final String PREF_NOTI_ICON = "noti_icon";
	public static final String PREF_LARGE_NOTI_ICON = "noti_large_icon";
	public static final String PREF_NOTI_SOUND = "noti_sound";
	public static final String PREF_NOTI_RECEIVER = "noti_receiver";
	public static final String PREF_NOTI_RES_ID = "noti_res_id";
	public static final String PREF_NOTI_TITLE_RES_ID = "noti_title_res_id";
	public static final String PREF_NOTI_CONTENT_RES_ID = "noti_content_res_id";

	public static final String PREF_DEVICECERT_STATUS = "pref_devicecert_status";

	public static final String PREF_ONEDAY_LOG = "pref_oneday_log";
	public static final String PREF_YESTERDAY = "pref_yesterday";

	public static final String PREF_FIRST_TIME = "pref_first_time";
	public static final String PREF_SECOND_TIME = "pref_second_time";
	// [end] Preferences

	// [start] database
	public static final String DB_UUID = "uuid";
	// [end]

	// [start] Popup Data Key
	public static final String POPUP_ACTIVITY = "popup_activity";
	// Popup Layout Data Key
	public static final String POPUP_BACKGROUND_RES_ID = "popup_background_res_id";
	public static final String POPUP_BACKGROUND_COLOR = "popup_background_color";

	// Top Layout Data Key
	public static final String TOP_LAYOUT_FLAG = "top_layout_flag";
	public static final String TOP_BACKGROUND_RES_ID = "top_background_res_id";
	public static final String TOP_BACKGROUND_COLOR = "top_background_color";
	public static final String TOP_TITLE_TYEP = "top_title_type";
	public static final String TOP_TITLE_TEXT = "text";
	public static final String TOP_TITLE_IMAGE = "image";
	public static final String TOP_TITLE_RES_ID = "top_title_res_id";
	public static final String TOP_TITLE_TEXT_DATA = "top_title_text_data";
	public static final String TOP_TITLE_TEXT_COLOR = "top_text_color";
	public static final String TOP_TITLE_SIZE = "top_text_size";

	// Content Layout Data Key
	public static final String CONTENT_BACKGROUND_RES_ID = "content_background_res_id";
	public static final String CONTENT_BACKGROUND_COLOR = "content_background_color";
	public static final String CONTENT_TEXT_COLOR = "content_text_color";
	public static final String CONTENT_TEXT_SIZE = "content_text_size";

	// Bottom Layout Data Key
	public static final String BOTTOM_TEXT_BTN_COUNT = "bottom_text_btn_count";
	public static final String BOTTOM_RICH_BTN_COUNT = "bottom_rich_btn_count";
	public static final String BOTTOM_BACKGROUND_RES_ID = "bottom_Background_res_id";
	public static final String BOTTOM_BACKGROUND_COLOR = "bottom_Background_color";
	public static final String BOTTOM_BTN_RES_ID = "bottom_btn_res_id";
	public static final String BOTTOM_BTN_TEXT_STATE = "bottom_btn_text_state";
	public static final String BOTTOM_BTN_TEXT_NAME = "bottom_btn_text_name";
	public static final String BOTTOM_BTN_TEXT_COLOR = "bottom_btn_text_color";
	public static final String BOTTOM_BTN_TEXT_SIZE = "bottom_btn_text_size";
	public static final String BOTTOM_BTN_TEXT_CLICKLISTENER = "bottom_btn_text_clickListener";
	public static final String BOTTOM_BTN_RICH_CLICKLISTENER = "bottom_btn_rich_clickListener";

	// XML Layout Data Key
	public static final String XML_LAYOUT_TEXT_RES_ID = "layout_text_res_id";
	public static final String XML_LAYOUT_RICH_RES_ID = "layout_rich_res_id";
	public static final String XML_TEXT_BUTTON_TYPE = "xml_text_button_type";
	public static final String XML_RICH_BUTTON_TYPE = "xml_rich_button_type";
	public static final String XML_TEXT_BUTTON_TAG_NAME = "xml_text_button_tag_name";
	public static final String XML_RICH_BUTTON_TAG_NAME = "xml_rich_button_tag_name";
	public static final String XML_AND_DEFAULT_FLAG = "xml_and_default_flag";

	public static final int TEXT_DEFAULT_SIZE = 15;
	public static final int TITLE_TEXT_DEFAULT_SIZE = 25;
	public static final int TEXTVIEW = 1;
	public static final int LINEARLAYOUT = 2;
	public static final int WEBVIEW = 3;
	public static final int PROGRESSBAR = 4;
	public static final int IMAGEVIEW = 5;
	public static final int BUTTON_VIEW = 6;
	public static final int IMAGE_BUTTON = 7;
	public static final int RELATIVELAYOUT = 8;
	// [end] Popup Data Key

	// [start] api
	// default encrypt key
	public static final String DEFAULT_ENC_KEY = "Pg-s_E_n_C_k_e_y";
	// API SERVER URL
	public static final String API_SERVER_URL = "http://119.207.76.92:8092/msg-api/";
	// time
	public static final long EXPIRE_RETAINED_TIME = 30/* minute */* 60 * 1000;
	public static final String PREF_LAST_API_TIME = "pref_last_api_time";
	// URL list
	public static final String API_DEVICE_CERT = "deviceCert.m";
	public static final String API_COLLECT_LOG = "collectLog.m";
	public static final String API_LOGIN_PMS = "loginPms.m";
	public static final String API_NEW_MSG = "newMsg.m";
	public static final String API_READ_MSG = "readMsg.m";
	public static final String API_CLICK_MSG = "clickMsg.m";
	public static final String API_SET_CONFIG = "setConfig.m";
	public static final String API_LOGOUT_PMS = "logoutPms.m";
	public static final String API_SET_MSGKIND_PMS = "setMsgKind.m";
	public static final String API_GET_MSGKIND_PMS = "getMsgKind.m";
	public static final String API_GET_SIGNKEY = "getSignKey.m";
	// pref
	public static final String PREF_APP_USER_ID = "pref_app_user_id";
	public static final String PREF_ENC_KEY = "pref_enc_key";
	public static final String PREF_APP_KEY_CHECK = "pref_app_key_check";
	public static final String PREF_APP_KEY = "pref_app_key";
	public static final String PREF_API_SERVER_CHECK = "pref_api_server_check";
	public static final String PREF_SERVER_URL = "pref_server_url";
	public static final String PREF_MQTT_SERVER_CHECK = "pref_mqtt_server_check";
	public static final String PREF_MQTT_SERVER_SSL_URL = "pref_mqtt_server_ssl_url";
	public static final String PREF_MQTT_SERVER_TCP_URL = "pref_mqtt_server_tcp_url";
	// request params(json) key
	public static final String KEY_API_DEFAULT = "d";
	public static final String KEY_APP_USER_ID = "id";
	public static final String KEY_ENC_PARAM = "data";
	// result params(json) key
	public static final String KEY_API_CODE = "code";
	public static final String KEY_API_MSG = "msg";
	// result code (server)
	public static final String CODE_SUCCESS = "000";
	public static final String CODE_NULL_PARAM = "100";
	public static final String CODE_WRONG_SIZE_PARAM = "101";
	public static final String CODE_WRONG_PARAM = "102";
	public static final String CODE_DECRPYT_FAIL = "103";
	public static final String CODE_PARSING_JSON_ERROR = "104";
	public static final String CODE_WRONG_SESSION = "105";
	public static final String CODE_ENCRYPT_ERROR = "106";
	public static final String CODE_API_INVALID = "109";
	public static final String CODE_INNER_ERROR = "200";
	public static final String CODE_EXTRA_ERROR = "201";
	// result code (client)
	public static final String CODE_SESSION_EXPIRED = "901";
	public static final String CODE_CONNECTION_ERROR = "902";
	public static final String CODE_NOT_RESPONSE = "903";
	public static final String CODE_URL_IS_NULL = "904";
	public static final String CODE_PARAMS_IS_NULL = "905";
	// device cert
	public static final String NO_TOKEN = "noToken";
	// new msg type
	public static final String TYPE_PREV = "P";
	public static final String TYPE_NEXT = "N";
	// auth status
	public static final String DEVICECERT_PENDING = "devicecert_pending";
	public static final String DEVICECERT_PROGRESS = "devicecert_progress";
	public static final String DEVICECERT_FAIL = "devicecert_fail";
	public static final String DEVICECERT_COMPLETE = "devicecert_complete";
	// [end] api
}
