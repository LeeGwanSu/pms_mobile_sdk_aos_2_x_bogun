package com.pms.sdk.api.request;

import org.json.JSONObject;

import android.content.Context;

import com.pms.sdk.api.APIManager.APICallback;
import com.pms.sdk.common.util.CLog;

public class GetMsgKind extends BaseRequest {

	public GetMsgKind(Context context) {
		super(context);
	}

	/**
	 * request
	 * 
	 * @param apiCallback
	 */
	public void request (final APICallback apiCallback) {
		try {
			apiManager.call(API_GET_MSGKIND_PMS, new JSONObject(), new APICallback() {
				@Override
				public void response (String code, JSONObject json) {
					if (CODE_SUCCESS.equals(code)) {
						requiredResultProc(json);
					}
					if (apiCallback != null) {
						apiCallback.response(code, json);
					}
				}
			});
		} catch (Exception e) {
			CLog.e(e.getMessage());
		}
	}

	/**
	 * required result proccess
	 * 
	 * @param json
	 */
	private boolean requiredResultProc (JSONObject json) {
		return true;
	}
}
